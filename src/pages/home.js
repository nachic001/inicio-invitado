import React from 'react';
import CarouselComp from '../components/inc/CarouselComp';

const Inicio =() => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '90vh',
        fontWeight : '400'
      }}
    >
      <h1>Profesores</h1> 
      <CarouselComp/>
    </div>
  );
};

export default Inicio;