import React from 'react';
import {
  Nav,
  NavLink,
  Bars,
  NavMenu,
  NavBtn,
  NavBtnLink
} from './NavbarElements';

const Navbar = () => {
  return (
    <>
      <Nav>
      <NavLink to='/'>
          <img src="https://media.discordapp.net/attachments/938596291569660034/1003185885937930250/descarga_3.png?width=225&height=76" alt='logo' />
        </NavLink>
        <Bars />
        <NavMenu>
            <NavLink href="home" activeStyle>
            Inicio
          </NavLink>
          <NavLink href="nosotros" activeStyle>
            Nosotros
          </NavLink>
          <NavLink href="planes" activeStyle>
            Planes
          </NavLink>
        </NavMenu>
        <NavBtn>
          <NavBtnLink href="https://alliancebjj.cl/login/">Ingresar</NavBtnLink>
        </NavBtn>
      </Nav>
    </>
  );
};

export default Navbar;