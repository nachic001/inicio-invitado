import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import mail from "../images/envelope.svg";
import "../css/card.css";

class CardEmail extends Component {
    render() {
        return (
            <Card bg='dark' text='light' style={{ width: 'auto',height: '180px' }}>
                <Card.Img class="card-img-top" src={mail} />
                <Card.Body>
                    <Card.Title>Email</Card.Title>
                    <Card.Text>
                    contacto@alliancesantiago.cl
                    </Card.Text>
                </Card.Body>
            </Card>
        );
    }
}

export default CardEmail;