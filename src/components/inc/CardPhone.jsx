import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import whasapp from '../images/whatsapp.svg';
import '../../App.css';
import "../css/card.css"
class CardPhone extends Component {
    render() {
        return (
            <Card bg='dark' text='light' style={{ width: 'auto',height: '180px' }} >
                <Card.Img variant="top" src={whasapp} />
                <Card.Body>
                    <Card.Title>WhatsApp</Card.Title>
                    <a href="https://api.whatsapp.com/send?phone=56948999358" class="stretched-link">+56 9 4899 9358</a>
                </Card.Body>
            </Card>
        );
    }
}

export default CardPhone;