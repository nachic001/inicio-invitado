import React, { Component } from 'react';
import { Carousel } from "react-bootstrap";

const URL = "http://127.0.0.1:3001/",
time = 5000;
let carouselTotal = "";

const fetchImages = async url => {
    try {
        let res = await fetch(url),
        json = await res.json();
        if(!res.ok) return 0;
        json.forEach(el => {
            if(el.Visibilidad === 1){
                carouselTotal += `<Carousel.Item interval={${time}}><img className="d-block w-100" src="data:image/jpg;base64,${el.Imagen.data}" alt="${el.Visibilidad}"/></Carousel.Item>`;
            }
        });
        return carouselTotal;
    } catch (err) {
        console.log(err)
    }
}

console.log(fetchImages(URL));

class CarouselImg extends Component {
    render() {
        return (
            <div>
                <Carousel></Carousel>
            </div>
        );
    }
}

export default CarouselImg;