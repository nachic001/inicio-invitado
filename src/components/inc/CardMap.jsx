import React, { Component } from 'react';
import {Card} from "react-bootstrap";
import geomap from '../images/geo-alt.svg';
import "../css/card.css"
class CardMap extends Component {
    render() {
        return (
            <div>
                <Card bg='dark' text='light' style={{ width: 'auto' ,height: '180px'}}>
                    <Card.Img variant="top" src={geomap} />
                    <Card.Body>
                        <Card.Title>Dirección</Card.Title>
                        <Card.Text>
                        Av. Hernando de Aguirre 162,Piso 2, Santiago, Chile
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

export default CardMap;