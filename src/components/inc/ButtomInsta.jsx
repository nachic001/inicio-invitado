import React, { Component } from 'react';
import { Button } from "react-bootstrap";
import insta from "../images/instagram.svg";
class ButtomInsta extends Component {
    render() {
        return (
            <Button size="lg" variant="dark" className="rounded-circle" href="https://www.instagram.com/alliancebjjsantiago/">
                <img src={insta} alt="insta" width="auto"/>
            </Button>
        );
    }
}

export default ButtomInsta;